import axios from 'axios'

class Requests {
  constructor () {
    this.url = 'http://api-forca.douglaspires.com:3000'
  }
  getWords () {
    return axios.get(`${this.url}/words`)
  }
  postWords (wordObject) {
    return axios.post(`${this.url}/words`, wordObject)
  }
  deleteWord (id) {
    return axios.delete(`${this.url}/words/${id}`)
  }
  getRandomWord (difficulty) {
    return axios.get(`${this.url}/words/random-word/${difficulty}`)
  }
  getRanking () {
    return axios.get(`${this.url}/ranking`)
  }
  postRanking (ranking) {
    return axios.post(`${this.url}/ranking`, ranking)
  }
}

export default new Requests()
