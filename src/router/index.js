import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/main/Main'
import RegisterWord from '@/components/record-word/RegisterWord'
import SelectDifficulty from '@/components/main/SelectDifficulty'
import Ranking from '@/components/ranking/Ranking'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main,
      children: [
        {
          path: '/register-word',
          name: 'register-word',
          component: RegisterWord
        },
        {
          path: '/change-difficulty',
          name: 'change-difficulty',
          component: SelectDifficulty
        },
        {
          path: '/ranking',
          name: 'ranking',
          component: Ranking
        }
      ]
    }
  ]
})
