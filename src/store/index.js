import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const IS_LOADING = 'IS_LOADING'
const THERE_IS_USER = 'THERE_IS_USER'
const DIFFICULTY_SELECTED = 'DIFFICULTY_SELECTED'
const IS_TIME_TO_PLAY = 'IS_TIME_TO_PLAY'

const state = {
  isLoading: true,
  thereIsUser: false,
  currentUserName: '',
  isDifficultySelected: false,
  difficultySelected: '',
  isTimeToPlay: false
}

const mutations = {
  [IS_LOADING] (state, context) {
    state.isLoading = false
  },
  [THERE_IS_USER] (state, context) {
    if (state.thereIsUser === false) {
      state.currentUserName = context
      state.thereIsUser = true
    } else {
      state.thereIsUser = false
    }
  },
  [DIFFICULTY_SELECTED] (state, context) {
    const difficulty = {}
    if (context === 'easy') difficulty.id = 0
    if (context === 'hard') difficulty.id = 1

    difficulty.name = context
    if (difficulty.id === 0) difficulty.alias = 'Fácil'
    if (difficulty.id === 1) difficulty.alias = 'Difícil'

    state.difficultySelected = difficulty.alias
    state.isDifficultySelected = true
    state.isTimeToPlay = true
  },
  [IS_TIME_TO_PLAY] (state, context) {
    if (state.isTimeToPlay === false) {
      state.isTimeToPlay = true
    } else {
      state.isTimeToPlay = false
    }
  }
}

const getters = {
  getIsLoading: state => state.isLoading,
  getThereIsUser: state => state.thereIsUser,
  getIsDifficultySelected: state => state.isDifficultySelected,
  getDifficultySelected: state => state.difficultySelected,
  getIsTimeToPlay: state => state.isTimeToPlay
}

const actions = {}

const store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})

export default store
