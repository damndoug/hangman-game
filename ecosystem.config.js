module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'forca',
      script    : './src/main.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    },
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'root',
      host : '153.92.209.59',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:damndoug/hangman-game.git',
      path : '/var/www/hangman/production/hangman',
      'post-deploy' : 'npm install && npm run build && pm2 reload ecosystem.config.js --env production'
    },
    dev : {
      user : 'root',
      host : '153.92.209.59',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:damndoug/hangman-game.git',
      path : '/var/www/hangman/development/hangman',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env dev',
      env  : {
        NODE_ENV: 'dev'
      }
    }
  }
};
